# MiOIB

Metaheuristics & Bio-Inspired Computing repository.  
Includes programs & reports for the course  
Winter 2017/2018  
Master Studies  
Intelligent Decission Support Systems  
Faculty of Computing, PUT  
  
To compile: simply use `make` in `metaheuristics/benchmark/` folder.  
To download test cases: use `metaheuristics/load.sh`  
To run: Go to `metaheuristics/benchmark/build` folder and run `./benchmark -c ../config.txt`  
To draw charts: Go to `metaheuristics/` folder and run `python drawCharts.py`  