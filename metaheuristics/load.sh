#!/bin/bash

PROBLEMS_ZIP_ADDR="http://anjos.mgi.polymtl.ca/qaplib/data.d/qapdata.tar.gz"
SOLUTIONS_ZIP_ADDR="http://anjos.mgi.polymtl.ca/qaplib/soln.d/qapsoln.tar.gz"

PROBLEMS_ZIP_FILE_NAME=$(basename "$PROBLEMS_ZIP_ADDR")
SOLUTIONS_ZIP_FILE_NAME=$(basename "$SOLUTIONS_ZIP_ADDR")

TEMP_DIR="temp"
DATA_DIR="data"
PROBLEMS_DIR="problems"
SOLUTIONS_DIR="solutions"

mkdir -p $TEMP_DIR

wget -P $TEMP_DIR $PROBLEMS_ZIP_ADDR
wget -P $TEMP_DIR $SOLUTIONS_ZIP_ADDR

mkdir -p $DATA_DIR

mkdir -p "$DATA_DIR/$PROBLEMS_DIR"
mkdir -p "$DATA_DIR/$SOLUTIONS_DIR"

tar -xf "$TEMP_DIR/$PROBLEMS_ZIP_FILE_NAME" -C "$DATA_DIR/$PROBLEMS_DIR"
tar -xf "$TEMP_DIR/$SOLUTIONS_ZIP_FILE_NAME" -C "$DATA_DIR/$SOLUTIONS_DIR"

rm -r $TEMP_DIR
