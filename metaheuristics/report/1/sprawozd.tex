%@descr: wzór sprawozdania, raportu lub pracy - nadaje się do przeróbek
%@author: Maciej Komosiński

\documentclass{article} 
\usepackage{polski} %moze wymagac dokonfigurowania latexa, ale jest lepszy niż standardowy babel'owy [polish] 
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage[utf8]{inputenc} 
\usepackage[OT4]{fontenc} 
\usepackage{graphicx,color} %include pdf's (and png's for raster graphics... avoid raster graphics!) 
\usepackage{url} 
\usepackage[pdftex,hyperfootnotes=false,pdfborder={0 0 0}]{hyperref} %za wszystkimi pakietami; pdfborder nie wszedzie tak samo zaimplementowane bo specyfikacja nieprecyzyjna; pod miktex'em po prostu nie widac wtedy ramek


\input{_ustawienia.tex}

%\title{Sprawozdanie z laboratorium:\\Metaheurystyki i Obliczenia Inspirowane Biologicznie}
%\author{}
%\date{}

\begin{document}

\input{_tytulowa}

\section*{Wstęp}

\emph{Qadratic Assignment Problem} (QAP) jest jednym z najbardziej znanych  \mbox{NP-trudnych} problemów kombinatorycznych. 

Definicja problemu jest następująca: dla zadanej liczby $n$ lokalizacji i $n$ podmiotów, macierzy odległości $\mathbb{D} [n \times n]$ oraz macierzy przepływu $\mathbb{F}[n \times n]$, znajdź taki jedno-jednoznaczny przydział lokalizacji do podmiotów $\pi$, aby zminimalizować łączny koszt transportu zadany wzorem:
\begin{equation}
f(\pi) = \sum_{i=1}^n \sum_{j=1}^n \mathbb{D}_{ij} \mathbb{F}_{\pi(i) \pi(j)}
\end{equation}

Jak łatwo zauważyć, QAP zawiera jako swój szczególny przypadek problem TSP. Przyjmijmy, że macierz $\mathbb{D}$ zawiera odległości pomiędzy miastami, a macierz $\mathbb{F}$ reprezentuje przejazd od jednego miasta do kolejnego. Posiada zatem jedynki tuż ponad przekątną i zera wszędzie indziej. Wtedy przydział $\pi$ reprezentuje przydział miast do kolejności, w której zostaną odwiedzone. W zależności od tego, czy macierz $\mathbb{D}$ jest symetryczna, czy asymetryczna, otrzymamy odpowiednio STSP lub ATSP.

Naturalną interpretacją problemu jest przydział oddziałów fabryk do dostępnych lokalizacji przy znanych zależnościach produkcyjnych i odległościach/kosztach transportu. Inne zastosowania obejmują np. rozmieszczenie elementów elektronicznych na płytce drukowanej, a w ogólności dowolne problemy rozmieszczenia zależnych od siebie elementów.

Złożoność wyczerpującego przeszukiwania (\emph{brute force}) byłaby proporcjonalna do liczby przyporządkowań (równiej liczbie permutacji, tj. $n!$) razy koszt ewaluacji pojedynczej permutacji. Nie stosując optymalizacji, otrzymamy $\Theta(n^2 *n!)$. 

Niestety, ponieważ QAP należy do kategorii problemów NP-trudnych (co wynika choćby z faktu, że można w nim zamodelować problem TSP tego samego rozmiaru), nie jest dla niego znany żaden algorytm wielomianowy. Biorąc pod uwagę jego ogólność i popularność, stanowi dobry przykład do porównywania różnych metod optymalizacji lokalnej i globalnej.

\subsection*{Wybrane instancje}

Do testowania wybrano podzbiór instancji dostępnych w \cite{QAPLib}.
Kryteria, którymi kierowano się przy selekcji to dostępność rozwiązania optymalnego, akceptowalny rozmiar oraz zróżnicowanie wyników eksperymentów.

\begin{enumerate}
\item nug12
\item els19
\item chr20a
\item bur26g
\item bur26h
\item kra30b
\item lipa40b
\item wil50
\end{enumerate}

\section{Konstrukcja algorytmów}

\subsection{Operator sąsiedztwa}

Na potrzeby algorytmów przeszukiwania, przyjęto sąsiedztwo 2-OPT. Oznacza ono, że sąsiadujące z danym przyporządkowaniem (permutacją) $\pi$ są takie przyporządkowania $\pi'$, w których zamienione zostały dowolne dwie pozycje, pozostawiając resztę niezmienioną.

Rozmiar sąsiedztwa można obliczyć, rozważając liczbę par elementów, które można ze sobą zamienić. Stąd $|N(\pi)| = \binom{n}{2}$ dla każdego $\pi$.

\subsection{Ewaluacja jakości rozwiązania}

Przy okazji opisu sąsiedztwa warto wspomnieć o relacji pomiędzy doborem sąsiedztwa a czasem jego ewaluacji. Oczywiście, dowolną permutację $\pi$ można ocenić w czasie $\Theta(n^2)$, jest to jednak mało satysfakcjonujące rozwiązanie dla dużych instancji. Jeśli jednak znana jest jakość rozwiązania $\pi$, można zdecydowanie szybciej obliczyć jakość każdego rozwiązania $\pi' \in N(\pi)$ powstałego poprzez zamianę lokalizacji $i$ oraz $j$ w czasie $\Theta(n)$ według poniższego wzoru:
\begin{align*}
f(\pi') &= f(\pi) + \Delta(\pi, i, j) \\
&= (\mathbb{D}_{ii} - \mathbb{D}_{jj}) * (\mathbb{F}_{\pi(j)\pi(i)} - \mathbb{F}_{\pi(i)\pi(j)}) \\
&+ (\mathbb{D}_{ij} - \mathbb{D}_{ji}) * (\mathbb{F}_{\pi(j)\pi(i)} - \mathbb{F}_{\pi(i)\pi(j)}) \\
&+ \sum_{g \neq i,j}(\mathbb{D}_{gi} - \mathbb{D}_{gj}) * (\mathbb{F}_{\pi(g)\pi(j)} - \mathbb{F}_{\pi(g)\pi(i)})\\
&+ (\mathbb{D}_{ig} - \mathbb{D}_{jg}) * (\mathbb{F}_{\pi(j)\pi(g)} - \mathbb{F}_{\pi(i)\pi(g)})
\end{align*}

W pracy \cite{DeltaTable} określono możliwe przyspieszenie zwane \emph{tablicą delta}, które jest korzystne przy stosowaniu algorytmu Steepest, który przeszukuje za każdym razem całe sąsiedztwo. Niestety, nie poprawia to całkowitej złożoności obliczeniowej, choć -- według autorów -- daje istotnie krótsze czasy wykonania.

W niniejszej pracy nie korzystano z tej optymalizacji, a algorytmy Greedy i Steepest oceniały rozwiązania z sąsiedztwa według powyższego wzoru w czasie liniowym.

\subsection{Heurystyka}

Przy opracowaniu heurystyki połączono dwa cele - utrzymanie elementu losowości oraz podejście zachłanne. Ostatecznie, opisana w sprawozdaniu heurystyka rozpoczyna od dostarczonej losowej permutacji, zatrzymuje przydział pierwszych trzech lokalizacji, a następnie przydziela po kolei lokalizacje w taki sposób, aby minimalizować koszta transportu do lokalizacji już przydzielonych. Ponieważ dla każdej lokalizacji należy przejrzeć wszystkich nie przydzielonych jeszcze producentów, a obliczenie kosztu zamiany ma złożoność liniową, cała heurystyka ma złożoność $\Theta(n^3)$.

\subsection{Bias algorytmu Greedy}

Algorytm Greedy powinien przeglądać sąsiedztwo aż do znalezienia pierwszego lepszego rozwiązania. Aby sukcesywnie przeglądać sąsiedztwo, zrezygnowano z losowania jego elementów, lecz przegląda się je zawsze w porządku leksykograficznym. Może to powodować bias algorytmu, który kieruje go ,,w określoną stronę'' w przestrzeni rozwiązań.

Jednak w podejściu Multi-Random Start, czyli wielokrotnej optymalizacji startując z losowych permutacji początkowych, powyższych efekt zostaje zniwelowany, ponieważ -- co wynika z symetrii -- jest tyle samo permutacji, w których dowolna para jest zamieniona miejscami. 

\subsection{Symulowane wyżarzanie}
W implementacji algorytmu symulowanego wyżarzania przyjęto następujące parametry:

\begin{itemize}
\item Stała długość łańcucha Markowa $L = \binom{n}{2}$
\item Potęgowo zmniejszający się parametr $c_k = \alpha^k c_0$
\item Współczynnik $\alpha = 0.8$
\item Początkowe prawdopodobieństwo przejścia do gorszego stanu $p = 0.999$
\item Warunek stopu = liczbę powtórzeń rozwiązania do końca algorytmu $R = 3$
\end{itemize}

Zastosowano również estymację średniej różnicy w jakości rozwiązania w ramach sąsiedztwa, próbkując losowo przestrzeń stanów (liczba próbek -- $50$). Na tej podstawie wyznaczono początkowy parametr $c_0$. Wykonano to jednak tylko raz dla danej instancji, ponieważ krok ten zajmował stosunkowo dużo czasu w porównaniu z samym przeszukiwaniem. Warto jednak zaznaczyć, że estymowanie przed każdym startem z losowego punktu zwracało znacznie lepsze wyniki, jednak trwało również istotnie dłużej.

\subsection{Przeszukiwanie Tabu}
W implementacji algorytmu przeszukiwania Tabu przyjęto następujące parametry:
\begin{itemize}
\item Rozmiar listy Master $|M| = n$
\item Regeneracja listy master co $n$ iteracji
\item Zakaz wykonania tego samego ruchu przez $\lfloor\frac{n}{2}\rfloor$ iteracji
\item Maksymalna dopuszczalna liczba iteracji bez polepszenia funkcji celu $I_{max} = 1000$
\end{itemize}

\begin{figure}
\begin{center}
\includegraphics[width=0.48\textwidth]{../../charts/nug12/bestAndWorst.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/els19/bestAndWorst.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/chr20a/bestAndWorst.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/bur26g/bestAndWorst.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/bur26h/bestAndWorst.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/kra30b/bestAndWorst.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/lipa40b/bestAndWorst.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/wil50/bestAndWorst.pdf}
\end{center}
\caption{Najlepsze oraz najgorsze znalezione rozwiązania algorytmów (1000 wspólnych startów z losowego przydziału)}
\label{fig:bestAndWorst}
\end{figure}

\section{Porównanie działania algorytmów}
\subsection{Odległość od optimum}
\subsubsection{Przypadek najlepszy i najgorszy}

Porównanie najlepszych oraz najgorszych znalezionych przez algorytmy rozwiązań przy (wspólnym) starcie z 1000 losowych punktów startowych znajduje się na Rysunku \ref{fig:bestAndWorst}.

Najbardziej odległe od optimum rozwiązania były generowane przez algorytm losowy oraz heurystykę. W zależności od instancji, czasami heurystyce udawało się znaleźć zadowalające rozwiązanie (np. \texttt{chr20a}), innym razem jednak zwracała rozwiązania gorsze, niż jakiekolwiek znalezione losowo (np. \texttt{nug12}). Ogólnie jednak znajdywane rozwiązania są dalece gorsze od algorytmów przeszukiwania lokalnego i metaheurystyk.

Pozostałe algorytmy działają znacznie lepiej. Praktycznie każdy z nich, uruchomiony z 1000 różnych punktów, otrzymał za którymś razem rozwiązanie optymalne, bądź bardzo mu bliskie. 

\begin{figure}
\begin{center}
\includegraphics[width=0.48\textwidth]{../../charts/nug12/avg.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/els19/avg.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/chr20a/avg.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/bur26g/avg.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/bur26h/avg.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/kra30b/avg.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/lipa40b/avg.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/wil50/avg.pdf}
\end{center}
\caption{Średni wynik algorytmów na poszczególnych instancjach z zaznaczonym na czerwono przedziałem o promieniu odchylenia standardowego (1000 wspólnych startów z losowego przydziału.}
\label{fig:average}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=0.48\textwidth]{../../charts/nug12/violin.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/els19/violin.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/chr20a/violin.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/bur26g/violin.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/bur26h/violin.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/kra30b/violin.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/lipa40b/violin.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/wil50/violin.pdf}
\end{center}
\caption{Rozkład znalezionych wyników algorytmów -- poziome linie oznaczają wartości skrajne (1000 wspólnych startów z losowego przydziału)}
\label{fig:distribution}
\end{figure}

\subsubsection{Przypadek średni i rozkład wyników}

Porównanie średnich znalezionych przez algorytmy rozwiązań przy (wspólnym) starcie z 1000 losowych punktów startowych, wraz z zaznaczonymi odchyleniami standardowymi znajduje się na Rysunku \ref{fig:average}.

Dodatkowo, rozkład osiąganych rezultatów można podejrzeć na wykresie typu \emph{Violin Plot} na Rysunku \ref{fig:distribution}.

Podobnie jak dla najlepszych i najgorszych wyników, najodleglejsze od optimum (średnio) rezultaty generują algorytm losowy oraz heurystyka. Warto zwrócić uwagę, że odchylenie standardowe algorytmu heurystycznego jest bardzo zróżnicowane w zależności od instancji -- przykładowo, dla \texttt{bur26h} algorytm za każdym razem generuje rozwiązania niemal jednakowej jakości.

Niestety, zaprezentowane wyniki nie pozwalają w jednoznaczny sposób porównać algorytmów przeszukiwania lokalnego i metaheurystyk. Nadal wyniki średnie uzyskiwane są bardzo podobne, a odchylenia standardowe istotnie mniejsze od rezultatów algorytmu losowego.

Na podstawie instancji \texttt{lipa40b} można by wnioskować, że odchylenia algorytmów symulowanego wyżarzania i przeszukiwania tabu są większe od algorytmów optymalizacji lokalnej. Byłby to wynik zgodny z intuicją przynajmniej dla SA, ale w wynikach dla pozostałych instancji ten wynik nie jest widoczny. Nie jest to więc wystarczająca przesłanka, by uznać taką zależność za istotnie zachodzącą.

\begin{figure}
\begin{center}
\includegraphics[width=0.48\textwidth]{../../charts/nug12/time_violin.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/els19/time_violin.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/chr20a/time_violin.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/bur26g/time_violin.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/bur26h/time_violin.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/kra30b/time_violin.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/lipa40b/time_violin.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/wil50/time_violin.pdf}
\end{center}
\caption{Rozkład czasów wykonania algorytmów}
\label{fig:time_distribution}
\end{figure}

\subsection{Czas działania}
Na Rysunku \ref{fig:time_distribution} można zobaczyć rozkład czasu działania poszczególnych algorytmów. Widać tu wyraźnie, że heurystyka oraz algorytm losowy są zdecydowanie najszybsze. Jest to spodziewane -- algorytm losowy musi jedynie wylosować jedną permutację, natomiast heurystyka ma określoną deterministycznie dla danego rozmiaru instancji liczbę kroków.

Za interesującą można uznać zależność pomiędzy czasami działania algorytmów przeszukiwania lokalnego Greedy oraz Steepest. Dla większości instancji można powiedzieć, że algorytm Steepest działa dłużej -- ale są też takie (np. \texttt{els19}), dla których Greedy wydaje się mieć gorszy czas działania. Można jednak jednoznacznie stwierdzić, że Greedy charakteryzuje się większym rozrzutem czasów działania.

Algorytm symulowanego wyżarzania działa krócej od algorytmów LS. Zdarzają mu się jednak wykonania zdecydowanie dłuższe, ale są one rzadkością.

Kilkakrotnie dłużej od pozostałych działa algorytm przeszukiwania Tabu. Niestety, nie można na tej podstawie wyciągać żadnych ogólnych wniosków - czas działania jest bardzo mocno uzależniony od dobranych parametrów. Manipulując nimi, można osiągnąć dowolnie długie lub krótkie czasy. To samo dotyczy również algorytmu symulowanego wyżarzania, nie ma natomiast takich parametrów w algorytmach LS.

\begin{figure}
\begin{center}
\includegraphics[width=0.48\textwidth]{../../charts/nug12/eff.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/els19/eff.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/chr20a/eff.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/bur26g/eff.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/bur26h/eff.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/kra30b/eff.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/lipa40b/eff.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/wil50/eff.pdf}
\end{center}
\caption{Rozkład miary efektywności dla algorytmów LS oraz metaheurystyk (1000 wspólnych startów z losowego przydziału)}
\label{fig:eff}
\end{figure}

\clearpage

\subsection{Efektywność (jakość w czasie)}

\subsubsection{Miara}

Zaproponowana przez autora miara efektywności zdefiniowana jest następująco:

\begin{equation}
E[time, result, optimum] = \log_2(1 + \frac{optimum}{result \times time})
\end{equation}

Wykazuje ona następujące cechy:
\begin{itemize}
\item dla $result = optimum$ oraz $time = 1$, otrzymujemy $\log_2(2) = 1$. Jest to najlepszy możliwy wynik (zakładamy, że czas jest wyrażony liczbą całkowitą dodatnią).
\item dla $time \rightarrow \infty$ otrzymujemy $\log_2 (1) = 0$. Podobnie dla $result \rightarrow \infty$.
\end{itemize}

Jest ona zatem znormalizowana. Ponadto, dzięki zlogarytmowaniu, rozrzut wyników jest mniejszy niż w przypadku bardziej liniowej miary. 

\subsubsection{Wyniki}

Porównanie efektywności algorytmów LS oraz metaheurystyk można obejrzeć na Rysunku \ref{fig:eff}. Celowo nie zamieszczono na nich wartości dla algorytmów losowego i heurystycznego. Ich efektywność -- według zaproponowanej miary -- była średnio o rząd wielkości lepsza w przypadku heurystki, oraz dodatkowo dwukrotnie lepsza w przypadku algorytmu losowego. Ponieważ podobna zależność zachodziła dla wszystkich badanych instancji, zdecydowano się nie umieszczać ich na wykresie w celu zachowania lepszej czytelności. 

Wyraźnie wydać, że najlepszą -- z prezentowanych czterech algorytmów -- efektywnością cechuje się algorytm Greedy. Znacznie mniejszą efektywność osiąga Steepest, a niemal zerową mają algorytmy metaheurystyczne. 

Niestety, również w tym przypadku nie można na podstawie tych wykresów wnioskować o uniwersalnych zależnościach pomiędzy tymi algorytmami. Jak zaznaczono już przy analizie czasu wykonania, metaheurystyki mają znaczną liczbę parametrów, którymi można manipulować w celu uzyskania zupełnie różnych czasów działania. Dodatkowym aspektem, który należy uwzględnić, jest fakt, że testowane instancje są niewielkie, a więc algorytmy LS często trafiały na rozwiązania niezbyt odległe od optymalnego.

\section{Porównanie algorytmów przeszukiwania lokalnego}

\begin{figure}
\begin{center}
\includegraphics[width=0.48\textwidth]{../../charts/nug12/stepsToStop.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/els19/stepsToStop.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/chr20a/stepsToStop.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/bur26g/stepsToStop.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/bur26h/stepsToStop.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/kra30b/stepsToStop.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/lipa40b/stepsToStop.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/wil50/stepsToStop.pdf}
\end{center}
\caption{Porównanie rozkładu liczby kroków (iteracji) niezbędnych do zatrzymania dla algorytmów przeszukiwania lokalnego}
\label{fig:stepsToStop}
\end{figure}

\subsection{Liczba kroków do zatrzymania}

Na Rysunku \ref{fig:stepsToStop} można zobaczyć rozkład liczby kroków niezbędnych do zatrzymania algorytmów Greedy oraz Steepest. Z wykresów widać wyraźnie, że algorytm Greedy potrzebuje znacznie większej liczby iteracji, aby osiągnąć optimum lokalne. Również wariancja jest istotnie większa niż w przypadku algorytmu Steepest. Zależność ta zachodzi dla wszystkich testowanych instancji.

Jest to rezultat spodziewany, ponieważ algorytm Steepest, przeszukując całe sąsiedztwo, intuicyjnie powinien szybciej znaleźć taki punkt, z którego nie da się już poprawić rozwiązania przeszukując sąsiedztwo.


\begin{figure}
\begin{center}
\includegraphics[width=0.48\textwidth]{../../charts/nug12/solutionsScanned.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/els19/solutionsScanned.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/chr20a/solutionsScanned.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/bur26g/solutionsScanned.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/bur26h/solutionsScanned.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/kra30b/solutionsScanned.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/lipa40b/solutionsScanned.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/wil50/solutionsScanned.pdf}
\end{center}
\caption{Porównanie rozkładu liczby przeszukanych rozwiązań przez algorytmy przeszukiwania lokalnego}
\label{fig:solutionsScanned}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=0.48\textwidth]{../../charts/nug12/initialVsFinal.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/els19/initialVsFinal.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/chr20a/initialVsFinal.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/bur26g/initialVsFinal.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/bur26h/initialVsFinal.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/kra30b/initialVsFinal.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/lipa40b/initialVsFinal.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/wil50/initialVsFinal.pdf}
\end{center}
\caption{Porównanie jakości początkowych i końcowych rozwiązań znalezionych przez algorytmy przeszukiwania lokalnego}
\label{fig:initialVsFinal}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=0.48\textwidth]{../../charts/nug12/multiRandomStart.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/els19/multiRandomStart.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/chr20a/multiRandomStart.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/bur26g/multiRandomStart.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/bur26h/multiRandomStart.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/kra30b/multiRandomStart.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/lipa40b/multiRandomStart.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/wil50/multiRandomStart.pdf}
\end{center}
\caption{Zależność najlepszego uzyskanego do danego momentu rozwiązania w funkcji liczby uruchomień (Multi-Random Start)}
\label{fig:multiRandomStart}
\end{figure}

\clearpage

\subsection{Liczba przeszukanych rozwiązań}
Rozkład całkowitej liczby przeszukanych rozwiązań przedstawia Rysunek \ref{fig:solutionsScanned}. Widać wyraźnie, że algorytm Greedy przeszukuje średnio mniejszą ilość rozwiązań -- pomimo, iż często wykonuje znacznie więcej iteracji. Można stąd wnioskować, że -- w większości przypadków -- są to iteracje przeszukujące niewielką część sąsiedztwa. Przeciwieństwem jest algorytm Steepest -- pomimo niewielkiej liczby iteracji, przegląda on w każdym kroku całe sąsiedztwo. W efekcie, na wybranych instancjach, algorytm Steepest przegląda średnio więcej rozwiązań niż Greedy (być może z wyjątkiem \texttt{els19}). Ma to również odzwierciedlenie w rozkładzie czasów wykonania, ponieważ jest on proporcjonalny do liczby rozpatrywanych permutacji.

\subsection{Porównanie jakości rozwiązań początkowych i końcowych}

Rysunek \ref{fig:initialVsFinal} przedstawia zależność pomiędzy jakością rozwiązania początkowego i końcowego. Wykresy są bardzo zróżnicowane. W przypadku instancji \texttt{chr20a}, \texttt{kra30b} oraz \texttt{wil50} widać raczej nieskorelowaną chmurę punktów, w której trudno wyróżnić zależności.

Inaczej sprawa się ma dla małych instancji, szczególnie \texttt{nug12}. Widać tu całe poziome pasma rozwiązań, które są prawdopodobnie punktami lokalnych optimów. Co ciekawe, takie pasma rozciągają się praktycznie przez cały zakres początkowych wylosowanych rozwiązań, co sugeruje, iż wiele różnych rozwiązań prowadzi do tego samego punktu. W przypadku instancji \texttt{els19} widać dodatkowo, że algorytm Steepest osiąga optimum globalne wielokrotnie częściej, niż algorytm Greedy.

Podobne pasma ujawniają się również dla instancji \texttt{bur26g} i \texttt{bur26h}. Być może można wyróżnić cechy instancji, dla których takie pasma występują wyraźniej (autor podejrzewa, że jest związane z globalną wypukłością).

Jeszcze jedną interesującą rzeczą są wyniki dla instancji \texttt{lipa40b}. Tutaj widać, że chmura punktów znajduje się bardzo daleko od rozwiązań o znacznie lepszej jakości (przypuszczalnie optimum globalnego). To sugeruje, że krajobraz funkcji celu jest bardzo trudny, a przeszukiwanie przypomina ,,szukanie igły w stogu siana''.

\subsection{Wielokrotny start z losowego punktu}

Wykres jakości najlepszego znalezionego rozwiązania w funkcji liczby uruchomień z losowego punktu jest przestawiony na Rysunku \ref{fig:multiRandomStart}. Co natychmiast widać, algorytmy optymalizacji sprawują się znacznie lepiej od przeszukiwania losowego. Jest to spodziewany efekt, gdyż szansa trafienia w jakiekolwiek minimum lokalne jest bliska zeru, natomiast algorytmy LS mogą poprawić niemal każde losowo wybrane początkowe rozwiązanie.

Kolejną widoczną zależnością jest szybka poprawa w ciągu pierwszych kilkudziesięciu iteracji i długa stagnacja w kolejnych iteracjach. Dotyczy to wszystkich przedstawionych algorytmów. 

Porównując tylko Greedy i Steepest, trudno wskazać na jednoznaczną przewagę któregokolwiek z nich. Na kilku instancjach Greedy osiągnął lepsze rozwiązanie niż Steepest, ale najczęściej rezultaty są bardzo zbliżone lub identyczne. Trudno również porównać szybkość zbieżności. Można by się spodziewać, że Steepest będzie szybciej znajdował dobre rozwiązanie, gdyż wybiera zawsze najlepszy element sąsiedztwa. Nie jest to jednak reguła, a efekt mocno zależy od krajobrazu funkcji celu. W przypadku instancji \texttt{lipa40b} widać wręcz przeciwną zależność -- algorytm Greedy znalazł praktycznie w pierwszym uruchomieniu rozwiązanie optymalne. Warto przy tym zaznaczyć, że oba algorytmy startowały z tego samego rozwiązania początkowego!

\clearpage

\begin{figure}
\begin{center}
\includegraphics[width=0.48\textwidth]{../../charts/nug12/similarity.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/els19/similarity.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/chr20a/similarity.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/bur26g/similarity.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/bur26h/similarity.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/kra30b/similarity.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/lipa40b/similarity.pdf}
\includegraphics[width=0.48\textwidth]{../../charts/wil50/similarity.pdf}
\end{center}
\caption{Zależność między odległością funkcji celu od rozwiązania optymalnego oraz miarą podobieństwa rozwiązania do optimum}
\label{fig:similarity}
\end{figure}

\clearpage

\section{Podobieństwo znajdywanych rozwiązań}

Na Rysunku \ref{fig:similarity} można zobaczyć podobieństwa otrzymywanych przez algorytmy rozwiązań do rozwiązania optymalnego. Jako miarę podobieństwa przyjęto liczbę wytwórców przypisanych do tej samej lokalizacji, co w rozwiązaniu optymalnym, podzieloną przez rozmiar instancji (normalizacja).

Na postawie wykresów można zauważyć, że średnie podobieństwo jest niewielkie. Nie dziwi to w przypadku algorytmu losowego. Jednakże znaczna część rozwiązań osiąganych przez algorytmy LS i metaheurystyki również ma niskie podobieństwo, pomimo znacznie lepszych wyników. 

Można jednak wnioskować w drugą stronę - wysokie podobieństwo oznacza, że dane rozwiązanie będzie istotnie lepsze od przeciętnego rozwiązania losowego.

Być może zastosowanie innej funkcji podobieństwa pozwoliłoby zobaczyć również inne zależności, jednakże trudno jest dobrać taką, która zachowuje ogólność (przykładowo, można założyć, że zamodelowany został problem TSP lub podobny -- lecz to właśnie utrata ogólności).

\section{Wnioski}

Na podstawie uzyskanych wyników można stwierdzić, że algorytmy przeszukiwania lokalnego mają sens, gdyż dają wyniki znacznie lepsze od przeszukiwania losowego bądź prostej heurystyki. Odnajdywane rozwiązania cechują się podobną jakością. Algorytm Steepest działa zwykle trochę dłużej niż Greedy, dzięki czemu osiąga wyższą efektywność. W podejściu Multi-Random Start, trudno wskazać zwycięzcę ze względu na szybkość zbieżności i ostateczną jakość. 

Algorytmy metaheurystyczne symulowanego wyżarzania oraz przeszukiwania Tabu -- przy ustalonych w eksperymencie parametrach -- dają wyniki porównywalnej jakości co przeszukiwanie lokalne. Jednocześnie, symulowane wyżarzanie działa trochę szybciej, natomiast przeszukiwanie Tabu znacznie wolniej. Sumaryczny efekt jest jednak niekorzystny dla metaheurystyk -- efektywność, według zaproponowanej w pracy miary, jest bardzo niska w porównaniu z pozostałymi algorytmami. Nie są to jednak konieczne relacje -- zależą ściśle od kontrolowanych czynników, zatem nie można w tej kwestii wyciągnąć wniosków natury ogólnej.

Rozwiązania otrzymywane przez algorytmy mają zróżnicowane podobieństwo do rozwiązania optymalnego. Choć można uznać, że wysokie podobieństwo sugeruje niewielką różnicę w jakości od optimum, w drugą stronę taka zależność zachodzi tylko w niektórych przypadkach. Dla wielu instancji, rozwiązania o wysokiej jakości zajmowały całe spektrum wartości funkcji podobieństwa. Jest to również element zależny od liczby optimów globalnych oraz globalnej wypukłości.

\subsection{Napotkane trudności}

Problemem, który nie pozwolił na rozstrzygające porównanie algorytmów LS i metaheurystycznych był problem z doborem parametrów tych drugich. Wiele z nich stanowi liczby, które poza kontekstem konkretnej instancji nie dają się wprost przełożyć ani na czas wykonania, ani na jakość ostatecznego rozwiązania. 

Pewnym rozwiązaniem był adaptacyjny dobór temperatury początkowej w algorytmie symulowanego wyżarzania. Początkowo, próbowano dokonywać takiego doboru przed każdym nowym uruchomieniem z nowego rozwiązania początkowego. Niestety, powodowało to bardzo duże zróżnicowanie czasów wykonania. Uzyskane wyniki okazywały się znacznie lepsze nawet dla większych instancji (nie przedstawionych w pracy). Mimo to, niedeterministyczny czas przetwarzania skłonił autora do zrezygnowania z tego podejścia. Poza tym, temperatura początkowa to tylko jeden z kilku parametrów, które -- w zasadzie -- zostały dobrane arbitralnie.

Wyzwaniem dla autora było również samo przygotowanie środowiska testowego. Jednym z wyznaczonych celów była powtarzalność wykonania poprzez zewnętrzne podanie ziarna dla generatora liczb pseudolosowych. Niestety, ograniczony czas nie pozwolił na zweryfikowanie, czy otrzymywane wyniki istotnie są identyczne. Program wykonujący eksperymenty został napisany w C++ i uruchomiony w systemie Linux. 

\subsection{Propozycje udoskonaleń}

Zdaniem autora, ciekawą analizą byłaby ocena jakości rozwiązań dla różnych parametrów algorytmów metaheurystycznych. Być może odnalezienie zależności w tej przestrzeni parametrów pozwoliłoby zaproponować rozwiązania podobne do adaptacyjnego doboru temperatury początkowej w symulowanym wyżarzaniu.

Z pewnością eksperyment można rozwinąć również o inne algorytmy przeszukiwania, na przykład algorytmy rojowe lub cząsteczkowe.

\clearpage

\bibliography{sprawozd}
\bibliographystyle{plainurl}

\clearpage 

\end{document}
