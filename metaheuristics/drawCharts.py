#!/usr/bin/env python

import os

import numpy as np
import matplotlib.pyplot as plt
import math

from enum import Enum

class Algorithm(Enum):
    __order__ = 'Random Heuristic Greedy Steepest SA TS'
    Random = "R"
    Heuristic = "H"
    Greedy = "G"
    Steepest = "S"
    SA = "SA"
    TS = "TS"

def loadInstanceNames(resultPath):
    return [x for x in os.listdir(resultPath) if os.path.isdir(os.path.join(resultPath, x))]

def loadResultsFile(fileName):
    print(fileName)
    resultsFile = open(fileName)
    scores = []
    times = []
    permutations = []
    for line in resultsFile.readlines():
        entry = line.replace("\n", "").split(" ")
        if len(entry[0]) == 0 or len(entry[1]) == 0:
            continue
        scores.append(int(entry[0]))
        times.append(int(entry[1]))
        permutations.append([int(x) for x in entry[2:]])

    return scores, times, permutations

def loadTraceFile(fileName):
    print(fileName)
    traceFile = open(fileName)
    solScanned = []
    trace = []
    currentTrace = []
    currentSolScanned = []
    for line in traceFile.readlines():
        if line == "START\n":
            if len(currentTrace) > 0:
                trace.append(currentTrace)
                solScanned.append(currentSolScanned)
            currentTrace = []
            currentSolScanned = []
        else:
            entries = list(filter(None, line.split(" ")))
            if len(entries) > 1:
                currentSolScanned.append(int(entries[1]))
            currentTrace.append(int(entries[0]))

    if len(currentTrace) > 0:
        trace.append(currentTrace)
        solScanned.append(currentSolScanned)

    return trace, solScanned

def loadSolutionFile(fileName):
    solutionFile = open(fileName)
    lines = solutionFile.readlines()
    firstLine = list(filter(None, lines[0].split(" ")))
    value = int(firstLine[1])
    permutation = []
    for line in lines[1:]:
        entries = list(filter(None, line.split(" ")))
        permutation.extend([int(x) for x in entries if len(x) > 0 and x != "\n"])
    return value, permutation

def prepareDirectory(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

def plotHistogram(results, name):
    resultValues = [x[0] for x in results]

def getInstanceChartDirectory(instanceName):
    chartsPath = 'charts'
    instanceChartDir = os.path.join(chartsPath, instanceName)
    prepareDirectory(instanceChartDir)
    return instanceChartDir

def savePlotToPDF(chartDirectory, name):
    plt.savefig(os.path.join(chartDirectory, name + ".pdf"))

def plotAverageWithStd(algorithms, results, instanceName, saveDir):
    plt.errorbar(range(len(algorithms)), [np.mean(line) for line in results], [np.std(line) for line in results], fmt='o', ecolor='r')
    plt.xticks(range(len(algorithms)), algorithms)
    plt.ylabel("Score")
    plt.title(instanceName)
    savePlotToPDF(saveDir, 'avg')
    plt.clf()

def plotStepsToStop(algorithms, traces, instanceName, saveDir):
    stepsToStop = [[len(x) for x in t] for t in traces]
    parts = plt.violinplot(stepsToStop)

    for pc in parts['bodies']:
        pc.set_facecolor('#FFFF00')
        pc.set_edgecolor('black')
        pc.set_alpha(1)

    plt.xticks(range(1, len(algorithms)+1), algorithms)
    plt.ylabel("Steps")
    plt.title(instanceName)
    savePlotToPDF(saveDir, 'stepsToStop')
    plt.clf()

def plotInitialVsFinal(algorithms, traces, instanceName, saveDir):

    colors = ['blue', 'red', 'green']
    handles = []
    i = 0

    for t in traces:
        initials = [x[0] for x in t]
        finals = [x[-1] for x in t]
        handle, = plt.plot(initials, finals, color = colors[i], marker = "o", markersize=1.5, linestyle='None', label = algorithms[i])
        handles.append(handle)
        i += 1

    plt.legend(handles=handles)

    plt.ylabel("Final score")
    plt.xlabel("Initial score")
    plt.title(instanceName)
    savePlotToPDF(saveDir, 'initialVsFinal')
    plt.clf()


def plotMultiRandomStart(algorithms, traces, instanceName, saveDir):
    handles = []
    s = 0
    for t in traces:
        lasts = [x[-1] for x in t]
        bests = []
        for i in range(len(lasts)):
            bestSoFar = min(lasts[:(i+1)])
            bests.append(bestSoFar)
        handle, = plt.plot(range(len(lasts)), bests, label = algorithms[s], lineStyle='--')
        handles.append(handle)
        s += 1

    plt.legend(handles=handles)
    plt.xlabel("Number of random starts")
    plt.ylabel("Best score so far")
    plt.title(instanceName)
    savePlotToPDF(saveDir, 'multiRandomStart')
    plt.clf()

def plotBestAndWorst(algorithms, results, instanceName, saveDir):
    plt.plot(range(len(algorithms)), [np.min(line) for line in results], 'g^')
    plt.plot(range(len(algorithms)-1), [np.max(line) for line in results[:-1]], 'bs')
    plt.xticks(range(len(algorithms)), algorithms)
    plt.ylabel("Score")
    plt.title(instanceName)
    savePlotToPDF(saveDir, 'bestAndWorst')
    plt.clf()

def plotWorst(algorithms, results, instanceName, saveDir):
    plt.plot(range(len(algorithms)), [np.max(line) for line in results], 'bs')
    plt.xticks(range(len(algorithms)), algorithms)
    plt.ylabel("Score")
    plt.title(instanceName)
    savePlotToPDF(saveDir, 'worst')
    plt.clf()

def plotViolin(algorithms, results, instanceName, saveDir):
    plt.violinplot(results)
    plt.xticks(range(1, len(algorithms)+1), algorithms)
    plt.ylabel("Score")
    plt.title(instanceName)
    savePlotToPDF(saveDir, 'violin')
    plt.clf()

def plotTimeViolin(algorithms, times, instanceName, saveDir):
    parts = plt.violinplot(times)

    for pc in parts['bodies']:
        pc.set_facecolor('#D43F3A')
        pc.set_edgecolor('black')
        pc.set_alpha(1)

    plt.xticks(range(1, len(algorithms)+1), algorithms)
    plt.ylabel("Wall time [ns]")
    plt.title(instanceName)
    savePlotToPDF(saveDir, 'time_violin')
    plt.clf()

def plotEfficiencyViolin(algorithms, results, times, instanceName, saveDir, opt):
    efficiencies = []
    for a in range(len(algorithms)):
        efficiencies.append([])
        for x in range(len(results[a])):
            efficiency = math.log(1 +opt / (results[a][x] * times[a][x]))
            efficiencies[a].append(efficiency)
    plt.errorbar(range(len(algorithms)), [np.mean(line) for line in efficiencies], [np.std(line) for line in efficiencies], fmt='*', color='orange', ecolor='black')
    plt.xticks(range(len(algorithms)), algorithms)
    plt.ylabel("Efficiency")
    plt.title(instanceName)
    savePlotToPDF(saveDir, 'eff')
    plt.clf()

def calculateSimilarity(permutationA, permutationB):
    matchingPositions = 0
    for i in range(len(permutationA)):
        if permutationA[i] == permutationB[i]:
            matchingPositions += 1

    return float(matchingPositions) / len(permutationA)

def plotSimilarity(algorithms, results, permutationTable, instanceName, saveDir, opt, optPermutation):

    markers = ['s', 'x', 'o', '+', '^', '^']
    colors = ['purple', 'blue', 'green', 'gray', 'orange', 'red']
    handles = []
    i = 0

    for i in range(len(permutationTable)):
        a = algorithms[i]
        permutations = permutationTable[i]

        scoreDistance = [x-opt for x in results[i]]
        similarities = [calculateSimilarity(optPermutation, p) for p in permutations]

        handle, = plt.plot(scoreDistance, similarities, color = colors[i], marker = markers[i], markersize=1, linestyle='None', label = algorithms[i])
        handles.append(handle)
        i += 1

    plt.legend(handles=handles)

    plt.ylabel("Similarity")
    plt.xlabel("Distance from optimum")
    plt.title(instanceName)
    savePlotToPDF(saveDir, 'similarity')
    plt.clf()

def plotSolutionsScanned(algorithms, solutionsScannedTable, instanceName, saveDir):
    stepsToStop = [[sum(x) for x in t] for t in solutionsScannedTable]
    parts = plt.violinplot(stepsToStop)

    for pc in parts['bodies']:
        pc.set_facecolor('#F0F0FF')
        pc.set_edgecolor('black')
        pc.set_alpha(1)

    plt.xticks(range(1, len(algorithms)+1), algorithms)
    plt.ylabel("Number of solutions")
    plt.title(instanceName)
    savePlotToPDF(saveDir, 'solutionsScanned')
    plt.clf()

def plotCharts(algorithms, results, times, permutationTable, instanceName, opt, optPermutation):
    saveDir = getInstanceChartDirectory(instanceName)

    plotAverageWithStd(algorithms, results, instanceName, saveDir)
    plotBestAndWorst(algorithms, results, instanceName, saveDir)
    #plotWorst(algorithms, results, instanceName, saveDir)
    plotViolin(algorithms, results, instanceName, saveDir)

    plotTimeViolin(algorithms[:-1], times, instanceName, saveDir)
    plotEfficiencyViolin(algorithms[2:-1], results[2:-1], times, instanceName, saveDir, opt)
    plotSimilarity(algorithms[:-1], results[:-1], permutationTable, instanceName, saveDir, opt, optPermutation)

    #plt.show()

def plotTraceCharts(algorithms, traces, solutionsScannedTable, instanceName, opt):
    saveDir = getInstanceChartDirectory(instanceName)

    plotStepsToStop(algorithms[1:], traces[1:], instanceName, saveDir)
    plotInitialVsFinal(algorithms[1:], traces[1:], instanceName, saveDir)
    plotMultiRandomStart(algorithms, traces, instanceName, saveDir)
    plotSolutionsScanned(algorithms[1:], solutionsScannedTable[1:], instanceName, saveDir)

def main():
    resultsPath = 'results'
    solutionsPath = 'data/solutions'

    instanceNames = loadInstanceNames(resultsPath)
    '''
    for instanceName in instanceNames:
        times = []
        results = []
        algorithms = []
        permutationTable = []
        for algorithm in Algorithm:
            filePath = os.path.join(resultsPath, instanceName, algorithm.value + ".txt")

            if not os.path.isfile(filePath):
                continue

            scores, runTimes, permutations = loadResultsFile(filePath)

            if len(scores) > 0:
                algorithms.append(algorithm.name)
                results.append(scores)
                times.append(runTimes)
                permutationTable.append(permutations)

        if len(results) > 0:
            solutionPath = os.path.join(solutionsPath, instanceName + ".sln")

            if os.path.isfile(solutionPath):
                algorithms.append('Optimal')
                optimalSolution, optPermutation = loadSolutionFile(solutionPath)
                results.append([optimalSolution])
                plotCharts(algorithms, results, times, permutationTable, instanceName, optimalSolution, optPermutation)
    '''
    traceAlgorithms = [Algorithm.Random, Algorithm.Greedy, Algorithm.Steepest]
    traceAlgorithmsNames = [x.name for x in traceAlgorithms]
    for instanceName in instanceNames:
        traces = []
        solutionsScannedTable = []
        for algorithm in traceAlgorithms:
            filePath = os.path.join(resultsPath, instanceName, algorithm.value + ".trace")

            if not os.path.isfile(filePath):
                continue

            trace, solutionsScanned = loadTraceFile(filePath)
            traces.append(trace)
            solutionsScannedTable.append(solutionsScanned)


        solutionPath = os.path.join(solutionsPath, instanceName + ".sln")

        if os.path.isfile(solutionPath):
            optimalSolution,_ = loadSolutionFile(solutionPath)
            plotTraceCharts(traceAlgorithmsNames, traces, solutionsScannedTable, instanceName, optimalSolution)

if __name__ == "__main__":
    main()

