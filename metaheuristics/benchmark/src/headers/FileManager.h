#pragma once

#include "Configuration.h"

#include <boost/filesystem.hpp>
#include <fstream>

using namespace std;

class FileManager {
	public:
		FileManager(Configuration *configuration);
		virtual ~FileManager();

		vector<string> getInstanceNames();
		ifstream* getProblemFile(string problemName);
		ifstream* getLoadFileFor(string problemName);
		ofstream* getResultFileFor(string problemName, string algorithmName);
		ofstream* getTraceFileFor(string problemName, string algorithmName);
		ofstream* getLoadFileForSave(string problemName);

	private:
		Configuration *configuration;
		vector<string> problemNames;

		vector<ifstream*> problemFiles;
		vector<ifstream*> loadFiles;
		vector<ofstream*> resultFiles;
		vector<ofstream*> traceFiles;
		vector<ofstream*> loadFilesToSave;

		void readAllProblemNames();
		void createDirectoriesForResults();
		void createDirectoryForResults(string problemName);
		void readProblemNamesFromFile();
};

