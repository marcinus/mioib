#pragma once

#include "IterativeQAPAlgorithm.h"

class GreedyLSQAPAlgorithm : public IterativeQAPAlgorithm {
	public:
		GreedyLSQAPAlgorithm() { name = "G"; shouldTraceScannedSolutions = true; }
		virtual ~GreedyLSQAPAlgorithm() { }

		virtual bool iteration();
};

