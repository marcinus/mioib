#pragma once

#include <map>
#include <boost/filesystem.hpp>
#include "ArgumentParser.h"

using namespace std;

static const map<string, ArgumentType> configFileKeys = {
	{ "seed", SEED },
	{ "instanceDescDir", INSTANCE_DESC_DIR },
	{ "instances", INSTANCE_LIST_FILE },
	{ "outputDir", OUTPUT_DIR },
	{ "loadDir", LOAD_DIR },
	{ "loadReadWrite", LOAD_READ_WRITE }
};

class Configuration {
	public:
		unsigned int seed;
		string instanceDescDir;
		string instanceListFileName;
		string outputDir;
		string loadDir;
		bool shouldReadLoad;
		bool shouldSaveLoad;

		Configuration();
		Configuration(string configFileName);
		Configuration(ArgumentParser *parser);
		virtual ~Configuration();

	private:
		void loadDefault();
		void loadFromFile(string configFileName);
		void loadFromCmdArguments(ArgumentParser *parser);
		void set(ArgumentType arg, string value);
		void correctIfPath(ArgumentType key, string &value, boost::filesystem::path baseDir);
};

