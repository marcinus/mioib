#pragma once

#include <climits>

#include "QAPAlgorithm.h"

class HeuristicQAPAlgorithm : public QAPAlgorithm {
	public:
		HeuristicQAPAlgorithm() { name = "H"; }
		virtual ~HeuristicQAPAlgorithm() { }
		
		void optimize();

		virtual void runAlgorithm() {
			optimize();
		}

		virtual void runAlgorithmWithTrace() {
			qualityTrace.clear();
			qualityTrace.push_back(currentSolution->quality);
			runAlgorithm();
			qualityTrace.push_back(currentSolution->quality);
		}
};

