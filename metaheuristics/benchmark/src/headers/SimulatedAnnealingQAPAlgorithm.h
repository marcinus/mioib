#include "IterativeQAPAlgorithm.h"

class SimulatedAnnealingQAPAlgorithm : public IterativeQAPAlgorithm {
	public:
		int k = 0;
		int L;
		double c;
		double alpha = 0.8;
		double p_start = 0.999;
		int deltaSample = 50;
		int MAX_REPETITIONS = 3;
		int repetitions = 0;

		SimulatedAnnealingQAPAlgorithm() { name = "SA"; prevSolution = new QAPSolution(0); }
		virtual ~SimulatedAnnealingQAPAlgorithm() { delete prevSolution; }

		virtual void setProblem(QAPProblem* problem);

		virtual double sampleSearchSpaceForAverageDelta(int sampleSiza);
		virtual double getAverageDeltaInNeighbourhood(QAPSolution *solution);
		virtual void runAlgorithm();
		virtual void runAlgorithmWithTrace();

		virtual bool epoch();
		virtual bool iteration();
	private:
		QAPSolution *prevSolution;
};

