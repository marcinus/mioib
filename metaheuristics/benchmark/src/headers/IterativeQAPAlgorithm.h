#pragma once

#include "QAPAlgorithm.h"

class IterativeQAPAlgorithm : public QAPAlgorithm {
	public:
		int solutionsScanned = 0;
		int maxIterations = -1;
		bool mustImprove = true;		

		virtual ~IterativeQAPAlgorithm() { }

		virtual bool iteration() = 0;

		virtual void runAlgorithm() {
			bool improving = true;

			if(maxIterations == -1) {
				do {
					improving = iteration();
				} while(improving);
			} else if(mustImprove) {
				for(int i = 0; i < maxIterations && improving; i++) {
					improving = iteration();
				}
			} else {
				for(int i = 0; i < maxIterations; i++) {
					iteration();
				}
			}
		}

		virtual void runAlgorithmWithTrace() {
			qualityTrace.clear();
			scannedSolutionsTrace.clear();
			qualityTrace.push_back(currentSolution->quality);
			scannedSolutionsTrace.push_back(0);
			bool improving = true;

			if(maxIterations == -1) {
				do {
					improving = iteration();
					qualityTrace.push_back(currentSolution->quality);
					scannedSolutionsTrace.push_back(solutionsScanned);
				} while(improving);
			} else if(mustImprove) {
				for(int i = 0; i < maxIterations && improving; i++) {
					improving = iteration();
					qualityTrace.push_back(currentSolution->quality);
					scannedSolutionsTrace.push_back(solutionsScanned);
				}
			} else {
				for(int i = 0; i < maxIterations; i++) {
					iteration();
					qualityTrace.push_back(currentSolution->quality);
					scannedSolutionsTrace.push_back(solutionsScanned);
				}
			}
		}
};
