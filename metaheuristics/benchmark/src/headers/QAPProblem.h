#pragma once

#include <fstream>
#include <iostream>

#include "QAPSolution.h"
#include "FileManager.h"

using namespace std;

class QAPProblem {
	public:
		QAPProblem(FileManager* fileManager, string problemName);
		virtual ~QAPProblem();

		string name;

		int n;
		int **D;
		int **F;

		long long delta(QAPSolution *solution, int i, int j);

		void printDToScreen();
		void printFToScreen();
};
