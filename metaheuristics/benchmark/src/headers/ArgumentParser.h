#pragma once

#include <map>

using namespace std;

enum ArgumentType { SEED, LOAD_DIR, INSTANCE_DESC_DIR, INSTANCE_LIST_FILE, OUTPUT_DIR, CONFIG_FILE, LOAD_READ_WRITE };

static const map<string, ArgumentType> allowedArguments = {
	{ "-s", SEED },
	{ "-d", INSTANCE_DESC_DIR },
	{ "-i", INSTANCE_LIST_FILE },
	{ "-o", OUTPUT_DIR },
	{ "-l", LOAD_DIR },
	{ "-c", CONFIG_FILE },
	{ "-r", LOAD_READ_WRITE }
};

class ArgumentParser {
	public:		
		map<ArgumentType, string> arguments;

		ArgumentParser(int argc, char* argv[]) {
			for(int i = 1; i < argc; i++) {
				auto it = allowedArguments.find(argv[i]);
				if(it != allowedArguments.end()) {
					arguments[it->second] = argv[++i];
				}
			}
		}

		bool has(ArgumentType argumentType) {
			auto it = arguments.find(argumentType);
			return it != arguments.end();
		}

		unsigned int getUInt(ArgumentType argumentType) {
			auto it = arguments.find(argumentType);
			return stoul(it->second);
		}

		string getString(ArgumentType argumentType) {
			auto it = arguments.find(argumentType);
			return (string) it->second;
		}

};

