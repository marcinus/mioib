#pragma once

#include "QAPProblem.h"
#include "QAPSolution.h"
#include "QAPEvaluator.h"

class QAPAlgorithm {
	public:
		string name;
		bool shouldTraceScannedSolutions = false;

		QAPProblem *problem;
		QAPSolution *currentSolution;

		QAPAlgorithm() {
			this->currentSolution = new QAPSolution(0);
		}

		virtual ~QAPAlgorithm() {
			delete currentSolution;
		}

		virtual void setProblem(QAPProblem *problem) {
			this->problem = problem;
			delete currentSolution;
			this->currentSolution = new QAPSolution(problem->n);
			randomStart();
		}

		virtual void setCurrentSolution(QAPSolution *solution) {
			delete currentSolution;
			currentSolution = new QAPSolution(*solution);
			QAPEvaluator::evaluateAndSet(problem, currentSolution);
		}

		virtual void randomStart() {
			currentSolution->random();
			QAPEvaluator::evaluateAndSet(problem, currentSolution);
		}

		virtual void runAlgorithm() = 0;
		virtual void runAlgorithmWithTrace() = 0;

		virtual vector<long long> getTrace() {
			return qualityTrace;
		}

		virtual vector<int> getScannedSolutionsTrace() {
			return scannedSolutionsTrace;
		}

	protected:
		vector<long long> qualityTrace;
		vector<int> scannedSolutionsTrace;
};

