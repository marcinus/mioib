#pragma once

#include "IterativeQAPAlgorithm.h"

#include <vector>
#include <map>
#include <utility>
#include <climits>

using namespace std;

class TabuSearchQAPAlgorithm : public IterativeQAPAlgorithm {
	public:
		int maximumNonImprovingIterationsTolerance = 1000;
		int tabuThreshold;
		int desiredMasterListSize;
		int regenMasterListPeriod;

		virtual bool iteration();
		virtual void runAlgorithm();
		virtual void runAlgorithmWithTrace();

		TabuSearchQAPAlgorithm();
		virtual ~TabuSearchQAPAlgorithm();

	private:
		QAPSolution* iteratingSolution;
		
		vector<pair<int, int> > masterList;
		map<pair<int, int>, int> tabuMap;
		
		int lastMasterListRegenIteration;
		int currentIteration;
		
		void regenerateMasterList();
};

