#pragma once

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cstring>

#include "Solution.h"

using namespace std;

class QAPSolution : public Solution {
	public:
		virtual void random();

		void swap(long long delta, int i, int j);
		void printToScreen();

		int n;
		long long quality;
		int *permutation;

		bool isEqual(QAPSolution* other);
		void copy(QAPSolution* original);
		string getPermutationAsString(string separator);
		
		QAPSolution(int n);
		QAPSolution(int n, long long quality, ifstream* loadFile);
		QAPSolution(const QAPSolution& copy);
		virtual ~QAPSolution();
	
	private:
		void fillOrderedPermutation();
};
