#pragma once

#include "QAPProblem.h"
#include "QAPSolution.h"

class QAPEvaluator {
	public:
		static long long evaluate(QAPProblem *problem, QAPSolution *solution);
		static void evaluateAndSet(QAPProblem *problem, QAPSolution *solution);
};
