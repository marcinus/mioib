#pragma once

#include "IterativeQAPAlgorithm.h"

class SteepestLSQAPAlgorithm : public IterativeQAPAlgorithm {
	public:
		SteepestLSQAPAlgorithm() { name = "S"; shouldTraceScannedSolutions = true; }
		virtual ~SteepestLSQAPAlgorithm() { }

		virtual bool iteration();
};

