#pragma once

#include <string>
#include <vector>
#include <boost/timer/timer.hpp>

#include "QAPAlgorithm.h"
#include "QAPProblem.h"
#include "FileManager.h"
#include "Configuration.h"

using namespace std;

struct less_than_size {
	inline bool operator() (const QAPProblem* problemA, const QAPProblem* problemB) {
		return (problemA->n < problemB->n);
	}
};

class QAPBenchmarkRunner {
	public:
		QAPBenchmarkRunner(FileManager *fileManager, Configuration *configuration) {
			this->fileManager = fileManager;
			this->configuration = configuration;
		}

		virtual ~QAPBenchmarkRunner() {
			for(auto &problem : problems) {
				delete problem;
			}
		}

		void printProgressToConsole(float progress, string extra);
		void flushConsoleLine(int positins);

		void addAlgorithm(QAPAlgorithm *algorithm);
		void setProblems(vector<string> problemNames);

		void benchmark(bool trace);

	private:
		Configuration* configuration;
		FileManager* fileManager;

		vector<QAPProblem*> problems;
		vector<QAPAlgorithm*> algorithms;
};

