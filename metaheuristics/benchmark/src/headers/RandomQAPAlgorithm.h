#pragma once

#include "IterativeQAPAlgorithm.h"

class RandomQAPAlgorithm : public IterativeQAPAlgorithm {
	public:
		RandomQAPAlgorithm(bool ignoreStartSolution);
		virtual ~RandomQAPAlgorithm();

		virtual void setProblem(QAPProblem *problem);
		virtual bool iteration();

	private:
		bool ignoreCurrentSolution;
		QAPSolution *nextSolution;
};

