#include "GreedyLSQAPAlgorithm.h"

bool GreedyLSQAPAlgorithm::iteration() {
	solutionsScanned = 0;
	for(int i = 0; i < problem->n; i++) {
		for(int j = i+1; j < problem->n; j++) {
			long long delta = problem->delta(currentSolution, i, j);
			solutionsScanned++;
			if(delta < 0) {
				currentSolution->swap(delta, i, j);
				return true;
			}
		}
	}

	return false;
}
