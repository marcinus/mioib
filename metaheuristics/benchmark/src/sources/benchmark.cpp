#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/timer/timer.hpp>

#include "ArgumentParser.h"
#include "Configuration.h"

#include "RandomQAPAlgorithm.h"
#include "GreedyLSQAPAlgorithm.h"
#include "SteepestLSQAPAlgorithm.h"
#include "HeuristicQAPAlgorithm.h"
#include "SimulatedAnnealingQAPAlgorithm.h"
#include "TabuSearchQAPAlgorithm.h"

#include "QAPBenchmarkRunner.h"

using namespace std;

int main(int argc, char* argv[]) {
	ArgumentParser argumentParser(argc, argv);
	Configuration configuration(&argumentParser);
	FileManager fileManager(&configuration);
	
	RandomQAPAlgorithm randomAlgorithm(true);
	GreedyLSQAPAlgorithm greedyAlgorithm;
	SteepestLSQAPAlgorithm steepestAlgorithm;
	HeuristicQAPAlgorithm heuristicAlgorithm;
	SimulatedAnnealingQAPAlgorithm saAlgorithm;
	TabuSearchQAPAlgorithm tsAlgorithm;

	QAPBenchmarkRunner runner(&fileManager, &configuration);

	runner.addAlgorithm(&randomAlgorithm);
	runner.addAlgorithm(&greedyAlgorithm);
	runner.addAlgorithm(&steepestAlgorithm);
	runner.addAlgorithm(&heuristicAlgorithm);
	runner.addAlgorithm(&saAlgorithm);
	runner.addAlgorithm(&tsAlgorithm);

	runner.setProblems(fileManager.getInstanceNames());

	runner.benchmark(true);
	
	return 0;
}
