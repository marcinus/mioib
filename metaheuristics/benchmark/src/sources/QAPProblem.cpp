#include "QAPProblem.h"

QAPProblem::QAPProblem(FileManager *fileManager, string problemName) {
	this->name = problemName;
	ifstream &file = *(fileManager->getProblemFile(problemName));
	file >> n;
	D = new int*[n];
	F = new int*[n];

	for(int i = 0; i < n; i++) {
		D[i] = new int[n];

		for(int j = 0; j < n; j++) {
			file >> D[i][j];
		}
	}

	for(int i = 0; i < n; i++) {
		F[i] = new int[n];
		
		for(int j = 0; j < n; j++) {
			file >> F[i][j];
		}
	}

	file.close();
}

QAPProblem::~QAPProblem() {
	for(int i = 0; i < n; i++) {
		delete[] D[i];
		delete[] F[i];
	}
	delete[] D;
	delete[] F;
}

long long QAPProblem::delta(QAPSolution* solution, int i, int j) {
	long long delta = 0;
	int *p = solution->permutation;
	delta += (D[i][i] - D[j][j]) * (F[p[j]][p[i]] - F[p[i]][p[j]]);
	delta += (D[i][j] - D[j][i]) * (F[p[j]][p[i]] - F[p[i]][p[j]]);
	for(int g = 0; g < n; g++) {
		if(g == i || g == j) continue;
		delta += (D[g][i] - D[g][j]) * (F[p[g]][p[j]] - F[p[g]][p[i]]);
		delta += (D[i][g] - D[j][g]) * (F[p[j]][p[g]] - F[p[i]][p[g]]);	
	}
	return delta;
}

void QAPProblem::printDToScreen() {
	for(int i = 0; i < n; i++) {
		for(int j = 0; j < n; j++) {
			cout << D[i][j] << " ";
		}
		cout << "\n";
	}
}

void QAPProblem::printFToScreen() {
	for(int i = 0; i < n; i++) {
		for(int j = 0; j < n; j++) {
			cout << F[i][j] << " ";
		}
		cout << "\n";
	}
}

