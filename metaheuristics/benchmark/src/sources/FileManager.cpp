#include "FileManager.h"

FileManager::FileManager(Configuration *configuration) {
	this->configuration = configuration;

	if(configuration->instanceListFileName.empty()) {
		readAllProblemNames();
	} else {
		readProblemNamesFromFile();
	}

	createDirectoriesForResults();

	if(!configuration->loadDir.empty()) {
		boost::filesystem::create_directories(boost::filesystem::path(configuration->loadDir));
	}
}

FileManager::~FileManager() {
	for(auto& file : problemFiles) {
		file->close();
		delete file;
	}
	for(auto& file : loadFiles) {
		file->close();
		delete file;
	}
	for(auto& file : resultFiles) {
		file->close();
		delete file;
	}
	for(auto& file : traceFiles) {
		file->close();
		delete file;
	}
	for(auto& file : loadFilesToSave) {
		file->close();
		delete file;
	}
}

vector<string> FileManager::getInstanceNames() {
	return problemNames;
}

ifstream* FileManager::getProblemFile(string problemName) {
	boost::filesystem::path problemDescDir(configuration->instanceDescDir);
	boost::filesystem::path problemDescFile(problemName + ".dat");

	ifstream* file = new ifstream((problemDescDir / problemDescFile).string());
	problemFiles.push_back(file);

	return file;
}

ifstream* FileManager::getLoadFileFor(string problemName) {
	boost::filesystem::path loadDir(configuration->loadDir);
	boost::filesystem::path loadFile(problemName + ".load");

	ifstream* file = new ifstream((loadDir / loadFile).string());
	loadFiles.push_back(file);

	return file;
}

ofstream* FileManager::getResultFileFor(string problemName, string algorithmName) {
	boost::filesystem::path outputDir(configuration->outputDir);
	boost::filesystem::path problemDir(problemName);
	boost::filesystem::path algorithmFile(algorithmName + ".txt");

	ofstream* file = new ofstream((outputDir / problemDir / algorithmFile).string());
	resultFiles.push_back(file);

	return file;
}

ofstream* FileManager::getTraceFileFor(string problemName, string algorithmName) {
	boost::filesystem::path traceDir(configuration->outputDir);
	boost::filesystem::path problemDir(problemName);
	boost::filesystem::path algorithmFile(algorithmName + ".trace");

	ofstream* file = new ofstream((traceDir / problemDir / algorithmFile).string());
	traceFiles.push_back(file);

	return file;
}

ofstream* FileManager::getLoadFileForSave(string problemName) {
	boost::filesystem::path loadDir(configuration->loadDir);
	boost::filesystem::path loadFile(problemName + ".load");

	ofstream* file = new ofstream((loadDir / loadFile).string());
	loadFilesToSave.push_back(file);

	return file;
}

void FileManager::readAllProblemNames() {
	boost::filesystem::directory_iterator it(configuration->instanceDescDir);
	boost::filesystem::directory_iterator endit;

	while(it != endit) {
		if(boost::filesystem::is_regular_file(*it) && it->path().extension() == ".dat") {
			problemNames.push_back(it->path().stem().string());
		}
		++it;
	}
}

void FileManager::createDirectoriesForResults() {
	for(auto &problemName : problemNames) {
		createDirectoryForResults(problemName);
	}
}

void FileManager::createDirectoryForResults(string problemName) {
	boost::filesystem::path resultDir(configuration->outputDir);
	boost::filesystem::path problemDir(problemName);
	boost::filesystem::create_directories(resultDir / problemDir);
}

void FileManager::readProblemNamesFromFile() {
	ifstream file(configuration->instanceListFileName);
	string problemName;
	while(file >> problemName && problemName != "") {
		problemNames.push_back(problemName);
	}
}

