#include "QAPEvaluator.h"

long long QAPEvaluator::evaluate(QAPProblem *problem, QAPSolution *solution) {
	if(problem->n != solution->n) {
		return -1;
	}

	int n = problem->n;

	long long objectiveFunction = 0;

	for(int i = 0; i < n; i++) {
		for(int j = 0; j < n; j++) {
			int pI = solution->permutation[i];
			int pJ = solution->permutation[j];
			objectiveFunction += problem->D[i][j] * problem->F[pI][pJ];
		}
	}

	return objectiveFunction;
}

void QAPEvaluator::evaluateAndSet(QAPProblem *problem, QAPSolution *solution) {
	long long evaluation = evaluate(problem, solution);
	solution->quality = evaluation;
}
