#include "QAPBenchmarkRunner.h"

#include <iomanip>
#include <algorithm>
#include <map>

void QAPBenchmarkRunner::addAlgorithm(QAPAlgorithm *algorithm) {
	algorithms.push_back(algorithm);
}

void QAPBenchmarkRunner::setProblems(vector<string> problemNames) {
	for(auto &problemName : problemNames) {
		QAPProblem *problem = new QAPProblem(fileManager, problemName);
		problems.push_back(problem);
	}
	sort(problems.begin(), problems.end(), less_than_size());
}

void QAPBenchmarkRunner::printProgressToConsole(float progress, string extra) {
	int barWidth = 65;
	cout << extra << " [";
	int pos = barWidth * progress;
	for (int i = 0; i < barWidth; ++i) {
		if (i < pos) cout << "=";
		else if (i == pos) cout << ">";
		else cout << " ";
	}
	cout << "] " << setfill('0') << setw(5) << fixed << setprecision(2) << progress * 100.0 << " %\r";
	cout.flush();
}

void QAPBenchmarkRunner::flushConsoleLine(int positions) {
	cout << "\r";
	for(int i = 0; i < positions; i++) {
		cout << " ";
	}
	cout << "\r";
}

void QAPBenchmarkRunner::benchmark(bool trace) {
	for(auto &problem : problems) {
		cout << "P: " << problem->name << endl;

		vector<QAPSolution*> startSolutions;

		int repetitions = 1000;

		if(configuration->shouldReadLoad) {
			ifstream* loadFile = fileManager->getLoadFileFor(problem->name);
			long long quality = 0;
			while((*loadFile) >> quality) {
				QAPSolution* solution = new QAPSolution(problem->n, quality, loadFile);
				startSolutions.push_back(solution);
			}

			repetitions = startSolutions.size();
		} else {
			srand(time(0));
			for(int i = 0; i < repetitions; i++) {
				QAPSolution *solution = new QAPSolution(problem->n);
				solution->random();
				startSolutions.push_back(solution);
			}
		}

		srand(configuration->seed);

		for(auto &algorithm : algorithms) {
			algorithm->setProblem(problem);

			vector<QAPSolution> solutions;
			vector<long long> times;

			ofstream *traceFilePointer;

			if(trace) {
				traceFilePointer = fileManager->getTraceFileFor(problem->name, algorithm->name);
			}

			for(int i = 0; i < repetitions; i++) {
				printProgressToConsole(float(i)/1000, algorithm->name);

				algorithm->setCurrentSolution(startSolutions[i]);
				
				boost::timer::cpu_timer t;
				if(trace) {
					algorithm->runAlgorithmWithTrace();
				} else {
					algorithm->runAlgorithm();
				}
				t.stop();

				solutions.push_back(*(algorithm->currentSolution));
				times.push_back(t.elapsed().wall);

				if(trace) {
					ofstream& traceFile = *traceFilePointer;
					vector<long long> qualityTrace = algorithm->getTrace();
					vector<int> scannedSolutionsTrace = algorithm->getScannedSolutionsTrace();
					traceFile << "START" << endl;
					for(int i = 0; i < qualityTrace.size(); i++) { 
						traceFile << qualityTrace[i];
						if(algorithm->shouldTraceScannedSolutions)
					       		traceFile << " " << scannedSolutionsTrace[i];
						traceFile << endl;
					}
				}

				flushConsoleLine(80);
			}

			ofstream& outputfile = *(fileManager->getResultFileFor(problem->name, algorithm->name));
	
			for(int i = 0; i < repetitions; i++) {
				outputfile << solutions[i].quality << " " << times[i] << " " << solutions[i].getPermutationAsString(" ") << "\n";
			}
		}

		if(configuration->shouldSaveLoad) {
			cout << "SAVING" << endl;
			ofstream& loadFile = *(fileManager->getLoadFileForSave(problem->name));
			for(int i = 0; i < repetitions; i++) {
				QAPEvaluator::evaluateAndSet(problem, startSolutions[i]);
				loadFile << startSolutions[i]->quality << " "<< startSolutions[i]->getPermutationAsString(" ") << endl;;
			}
		}

		for(int i = 0; i < repetitions; i++) {
			delete startSolutions[i];
		}
	}
}

