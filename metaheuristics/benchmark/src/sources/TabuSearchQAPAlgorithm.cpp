#include "TabuSearchQAPAlgorithm.h"

TabuSearchQAPAlgorithm::TabuSearchQAPAlgorithm() {
	iteratingSolution = new QAPSolution(0);
	name = "TS";
}

TabuSearchQAPAlgorithm::~TabuSearchQAPAlgorithm() {
	delete iteratingSolution;
}

void TabuSearchQAPAlgorithm::runAlgorithm() {
	tabuThreshold = problem->n / 2;
	desiredMasterListSize = problem->n;
	regenMasterListPeriod = problem->n;
	iteratingSolution->copy(currentSolution);
	currentIteration = 0;
	int nonImprovingIterations = 0;
	tabuMap.clear();
	regenerateMasterList();
	do {
		bool improving = iteration();
		if(improving) nonImprovingIterations = 0;
		else nonImprovingIterations++;
	} while(nonImprovingIterations < maximumNonImprovingIterationsTolerance); 
}

void TabuSearchQAPAlgorithm::runAlgorithmWithTrace() {
	tabuThreshold = problem->n / 2;
	desiredMasterListSize = problem->n;
	regenMasterListPeriod = problem->n;
	iteratingSolution->copy(currentSolution);
	qualityTrace.clear();
	tabuMap.clear();
	qualityTrace.push_back(currentSolution->quality);
	currentIteration = 0;
	int nonImprovingIterations = 0;
	regenerateMasterList();
	do {
		bool improving = iteration();
		if(improving) nonImprovingIterations = 0;
		else nonImprovingIterations++;
		qualityTrace.push_back(currentSolution->quality);
	} while(nonImprovingIterations < maximumNonImprovingIterationsTolerance); 
}

bool TabuSearchQAPAlgorithm::iteration() {
	if(currentIteration - lastMasterListRegenIteration > regenMasterListPeriod) {
		regenerateMasterList();
	}
	long long bestDelta = LLONG_MAX;
	pair<int, int> bestMove;
	for(auto& x : masterList) {
		if(tabuMap.count(x) == 0 || currentIteration - tabuMap[x] > tabuThreshold) {
			long long delta = problem->delta(iteratingSolution, x.first, x.second);
			if(delta < bestDelta) {
				bestDelta = delta;
				bestMove = x;
			}
		}
	}

	iteratingSolution->swap(bestDelta, bestMove.first, bestMove.second);

	tabuMap[bestMove]++;
	currentIteration++;

	if(iteratingSolution->quality < currentSolution->quality) {
		currentSolution->copy(iteratingSolution);
		return true;
	}

	return false;
}

void TabuSearchQAPAlgorithm::regenerateMasterList() {
	vector<pair<int, pair<int, int> > > newMasterList;

	for(int i = 0; i < problem->n; i++) {
		for(int j = i+1; j < problem->n; j++) {
			long long delta = problem->delta(iteratingSolution, i, j);
			newMasterList.push_back(make_pair(delta, make_pair(i, j)));
		}
	}
	
	sort(newMasterList.begin(), newMasterList.end());
	
	masterList.clear();

	for(int i = 0; i < newMasterList.size() && i < desiredMasterListSize; i++) {
		masterList.push_back(newMasterList[i].second);
	}

	lastMasterListRegenIteration = currentIteration;
}

