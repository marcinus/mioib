#include "RandomQAPAlgorithm.h"

RandomQAPAlgorithm::RandomQAPAlgorithm(bool ignoreStartSolution) {
	name = "R";
	maxIterations = 1;
	mustImprove = false;
	nextSolution = new QAPSolution(0);
	this->ignoreCurrentSolution = ignoreStartSolution;
}

RandomQAPAlgorithm::~RandomQAPAlgorithm() {
	delete nextSolution;
}

void RandomQAPAlgorithm::setProblem(QAPProblem* problem) {
	IterativeQAPAlgorithm::setProblem(problem);
	delete nextSolution;
	nextSolution = new QAPSolution(problem->n);
}

bool RandomQAPAlgorithm::iteration() {
	if(ignoreCurrentSolution) {
		ignoreCurrentSolution = false;
		randomStart();
		return true;
	} else {
		nextSolution->random();
		QAPEvaluator::evaluateAndSet(problem, nextSolution);
		if(nextSolution->quality < currentSolution->quality) {
			QAPSolution* swap = currentSolution;
			currentSolution = nextSolution;
			nextSolution = swap;
			return true;	
		}
		return false;
	}
}

