#include "SimulatedAnnealingQAPAlgorithm.h"

#include <iomanip>
#include <cmath>

double SimulatedAnnealingQAPAlgorithm::sampleSearchSpaceForAverageDelta(int sampleSize) {
	double avg = 0.0;

	QAPSolution* solution = new QAPSolution(problem->n);

	for(int i = 0; i < sampleSize; i++) {
		solution->random();
		QAPEvaluator::evaluateAndSet(problem, solution);
		avg += getAverageDeltaInNeighbourhood(solution);
	}

	avg /= sampleSize;

	delete solution;
	return avg;
}

double SimulatedAnnealingQAPAlgorithm::getAverageDeltaInNeighbourhood(QAPSolution *solution) {
	int n = solution->n;
	long long startQuality = solution->quality;
	vector<long long> qualities;
	qualities.push_back(startQuality);
	for(int i = 0; i < n; i++) {
		for(int j = i+1; j < n; j++) {
			long long quality = startQuality + problem->delta(solution, i, j);
			qualities.push_back(quality);
		}
	}

	sort(qualities.rbegin(), qualities.rend());

	double avg = 0.0;

	for(int i = 0; i < qualities.size(); i++) {
		avg += ((int)qualities.size()-1-2*i) * qualities[i];
	}

	double divider = qualities.size() * (qualities.size()-1) / 2;

	avg /= divider;

	return avg;
}

void SimulatedAnnealingQAPAlgorithm::setProblem(QAPProblem* problem) {
	IterativeQAPAlgorithm::setProblem(problem);
	double delta = sampleSearchSpaceForAverageDelta(deltaSample);
	c = -delta/log(p_start);
	L = problem->n * (problem->n - 1) / 2;
	cout << "Params: L=" << L << ", delta=" << delta << ", c=" << c << endl;
}

void SimulatedAnnealingQAPAlgorithm::runAlgorithm() {
	do {
		epoch();
	} while(repetitions < MAX_REPETITIONS);
}

void SimulatedAnnealingQAPAlgorithm::runAlgorithmWithTrace() {
	qualityTrace.clear();
	qualityTrace.push_back(currentSolution->quality);
	do {
		epoch();
		qualityTrace.push_back(currentSolution->quality);
	} while(repetitions < MAX_REPETITIONS);
}

bool SimulatedAnnealingQAPAlgorithm::epoch() {
	prevSolution->copy(currentSolution);

	for(int i = 0; i < L; i++) {
		iteration();
	}

	c = alpha * c;
	
	if(prevSolution->isEqual(currentSolution)) {
		repetitions++;
	} else {
		repetitions = 0;
	}

	return currentSolution->quality < prevSolution->quality;
}

bool SimulatedAnnealingQAPAlgorithm::iteration() {
	long long quality = currentSolution->quality;

	int i = rand() % problem->n;
	int j = rand() % (problem->n-1);
	if(j >= i) j++;

	long long delta = problem->delta(currentSolution, i, j);

	if(delta < 0) {
		currentSolution->swap(delta, i, j);
	} else {
		double p = exp(-(double) delta / c);
		double flip = ((double) rand() / (RAND_MAX));
		if(delta == 0 && c < 0.01) p = 0;
		if(p > flip) {
			currentSolution->swap(delta, i, j);
		}
	}

	return currentSolution->quality < quality;
}

