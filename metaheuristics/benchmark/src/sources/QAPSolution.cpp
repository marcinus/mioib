#include "../headers/QAPSolution.h"

QAPSolution::QAPSolution(int n) {
	this->n = n;
	permutation = new int[n];
	fillOrderedPermutation();
}

QAPSolution::~QAPSolution() {
	delete[] permutation;
}

QAPSolution::QAPSolution(int n, long long quality, ifstream* loadFile) {
	this->n = n;
	this->quality = quality;
	ifstream& file = *loadFile;
	permutation = new int[n];
	for(int i = 0; i < n; i++) {
		file >> permutation[i];
		permutation[i]--;
	}
}

QAPSolution::QAPSolution(const QAPSolution& copy) {
	n = copy.n;
	quality = copy.quality;
	permutation = new int[n];
	memcpy(permutation, copy.permutation, n*sizeof(int));
}

void QAPSolution::random() {
	int newIndex = 0, swapTempVar = 0;
	for(int i = 0; i < n; i++) {
		newIndex = i + rand() % (n-i);
		swapTempVar = permutation[i];
		permutation[i] = permutation[newIndex];
		permutation[newIndex] = swapTempVar;
	}
}

void QAPSolution::swap(long long delta, int i, int j) {
	quality += delta;
	int swap = permutation[i];
	permutation[i] = permutation[j];
	permutation[j] = swap;
}

void QAPSolution::printToScreen() {
	for(int i = 0; i < n; i++) {
		cout << permutation[i] << " ";
	}
	cout << "\n";
}

void QAPSolution::copy(QAPSolution* original) {
	if(original->n != n) {
		delete[] permutation;
		n = original->n;
		permutation = new int[n];
	}
	quality = original->quality;
	memcpy(permutation, original->permutation, n*sizeof(int));
}

bool QAPSolution::isEqual(QAPSolution* other) {
	if(other->n != n) return false;
	if(other->quality != quality) return false; 
	for(int i = 0; i < n; i++) {
		if(other->permutation[i] != permutation[i]) return false;
	}

	return true;
}

void QAPSolution::fillOrderedPermutation() {
	for(int i = 0; i < n; i++) {
		permutation[i] = i;
	}
}

string QAPSolution::getPermutationAsString(string separator) {
	string result = "";
	if(n > 0) {
		for(int i = 0; i < n-1; i++) {
			result += to_string(permutation[i]+1);
			result += separator;
		}
		result += to_string(permutation[n-1]+1);
	}
	return result;
}
