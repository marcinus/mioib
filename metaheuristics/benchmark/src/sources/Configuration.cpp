#include "Configuration.h"

#include <fstream>
#include <boost/filesystem.hpp>

Configuration::Configuration() {
	loadDefault();
}

Configuration::Configuration(string configFileName) {
	loadDefault();
	loadFromFile(configFileName);
}

Configuration::Configuration(ArgumentParser *parser) {
	loadDefault();
	if(parser->has(CONFIG_FILE)) {
		loadFromFile(parser->getString(CONFIG_FILE));
	}
	loadFromCmdArguments(parser);
}

Configuration::~Configuration() { }

void Configuration::loadDefault() {
	set(SEED, to_string(time(0)));
	set(INSTANCE_DESC_DIR, "../../data/problems");
	set(INSTANCE_LIST_FILE, "");
	set(OUTPUT_DIR, "../../results");
	set(LOAD_DIR, "");
	set(LOAD_READ_WRITE, "");
}

void Configuration::loadFromFile(string configFileName) {
	ifstream configFile(configFileName);
	boost::filesystem::path configBaseFilePath(configFileName);
	boost::filesystem::path configBaseDir = configBaseFilePath.parent_path();

	string line;

	while(getline(configFile, line)) {
		size_t eqPosition = line.find("=");
		if(eqPosition != string::npos) {
			string key = line.substr(0, eqPosition);
			string value = line.substr(eqPosition + 1);
			
			auto it = configFileKeys.find(key);
			if(it != configFileKeys.end()) {
				correctIfPath(it->second, value, configBaseDir);
				set(it->second, value);
			}
		}
	}
}

void Configuration::loadFromCmdArguments(ArgumentParser *parser) {
	for(auto &entry : parser->arguments) {
		set(entry.first, entry.second);
	}
}

void Configuration::correctIfPath(ArgumentType key, string &value, boost::filesystem::path basePath) {
	switch(key) {
		case INSTANCE_DESC_DIR:
		case INSTANCE_LIST_FILE:
		case OUTPUT_DIR:
		case LOAD_DIR:
			value = (basePath / boost::filesystem::path(value)).string();
		break;
	}
}

void Configuration::set(ArgumentType arg, string value) {
	if(arg == SEED) {
		seed = stoul(value);
	} else if(arg == INSTANCE_DESC_DIR) {
		instanceDescDir = value;
	} else if(arg == INSTANCE_LIST_FILE) {
		instanceListFileName = value;
	} else if(arg == OUTPUT_DIR) {
		outputDir = value;
	} else if(arg == LOAD_DIR) {
		loadDir = value;
	} else if(arg == LOAD_READ_WRITE) {
		if(value == "read") {
			shouldReadLoad = true;
			shouldSaveLoad = false;
		} else if(value == "write") {
			shouldReadLoad = false;
			shouldSaveLoad = true;
		} else {
			shouldReadLoad = shouldSaveLoad = false;
		}
	}
}
