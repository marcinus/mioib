#include "HeuristicQAPAlgorithm.h"

void HeuristicQAPAlgorithm::optimize() {
	int **D = problem->D;
	int **F = problem->F;
	int *p = currentSolution->permutation;
	for(int i = 0; i < currentSolution->n; i++) {
		long long bestUpdate = LLONG_MAX;
		int bestIndex = -1;
		for(int j = i; j < currentSolution->n; j++) {
			long long update = 0;
			for(int k = 0; k < i; k++) {
				update += D[i][k] * F[p[j]][p[k]];
				update += D[k][i] * F[p[k]][p[j]];
			}
			update += D[i][i] * F[p[j]][p[j]];

			if(update < bestUpdate) {
				bestUpdate = update;
				bestIndex = j;
			}
		}

		int swap = p[i];
		p[i] = p[bestIndex];
		p[bestIndex] = swap;
	}
	QAPEvaluator::evaluateAndSet(problem, currentSolution);
}

