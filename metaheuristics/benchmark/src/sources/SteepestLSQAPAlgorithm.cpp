#include "SteepestLSQAPAlgorithm.h"

bool SteepestLSQAPAlgorithm::iteration() {
	solutionsScanned = 0;
	long long bestDelta = 0;
	int bestI = 0, bestJ = 0;
	for(int i = 0; i < problem->n; i++) {
		for(int j = i+1; j < problem->n; j++) {
			solutionsScanned++;
			long long currentDelta = problem->delta(currentSolution, i, j);
			if(currentDelta < bestDelta) {
				bestDelta = currentDelta;
				bestI = i; bestJ = j;
			}
		}
	}

	if(bestDelta < 0) {
		currentSolution->swap(bestDelta, bestI, bestJ);
		return true;
	}

	return false;
}
